#include <string>
#include "stdafx.h"
#include "stdsoap2.h"
#include "soapcomcarService.h"
#include "comcar.nsmap"
#include "ComCarService.h"
#include <iostream>

int comcarService::get_plate_number(std::string& result)
{
	return SOAP_ERR;
}
int comcarService::open_url(std::string url, std::string user, std::string pass, std::string& state)
{
	return SOAP_ERR;
}
int comcarService::close()
{
	return SOAP_ERR;
}


int ComCarService::open_url(std::string url, std::string user, std::string pass, std::string& state)
{
	if (recognizer.openURL(url, user, pass))
	{
		recognizer.start();
		state = "OK";
	}
	else
	{
		state = "FAIL";
	}

	return SOAP_OK;
}

int ComCarService::close()
{
	recognizer.stop();
	return SOAP_OK;
}

int ComCarService::get_plate_number(std::string& result)
{
	if (recognizer.hasPlate())
	{
		result = recognizer.getPlate();
		std::cout << result;
	}
	else
		result = "none";

	return SOAP_OK;
}
