#include "stdafx.h"
#include "ACS.h"
#include "Recognizer.h"

#include <sstream>
#include <iostream>
#include <fstream>

using namespace std;

ACS::ACS( string _path)
{
	path = _path;
}


ACS::~ACS()
{
}


const string& ACS::check(const string& plate)
{
	auto found = find_morph(known, plate);

	if(found != known.end())
		return *found;



	return plate;
}


void ACS::arrival(const string& plate, const Recognizer& rec)
{
	auto found = find_morph(known, plate);
	if (found == known.end())
		known.push_back(plate);

	found = find_morph(trip, plate);
	if (found == trip.end())
	{
		trip.push_back(plate);
		cout << timestamp() << " arrival " << plate << endl;
		rec.saveImagePlate(path+"\\in\\"+filename_ts(plate));
		print();
	}
}
void ACS::departure(const string& plate, const Recognizer& rec)
{
	auto found = find_morph(trip, plate);
	if (found != trip.end()) //
	{
		trip.erase(found);
	}
	else
	{
		trip.push_back(string("-") + plate);
	}

	cout << timestamp() << " departure " << plate << endl;
	rec.saveImagePlate(path + "\\out\\" + filename_ts(plate));
	print();
}


void ACS::print()
{
	cout << "=====================" << endl;
	for (auto idx = trip.begin(); idx < trip.end(); idx++)
	{
		cout << *idx << endl;
	}
	cout << "---------------------" << endl;
}


const vector<string>::const_iterator find_morph(const vector<string>& list, string elem, cost_type delta)
{
	cost_type min_dst = 99.0;
	auto result = list.end();

	for (auto iter = list.begin(); iter < list.end(); iter++)
	{
		auto dst = edit_distance(*iter, elem);
		if (dst <= delta)
		{
			if (dst < min_dst)
			{
				min_dst = dst;
				result = iter;
			}
		}
	}
	return result;
}




cost_type insertion_cost(std::string::value_type ch) {
	return 1;
}

cost_type removal_cost(std::string::value_type ch) {
	return 1;
}

cost_type update_cost(std::string::value_type lhs, std::string::value_type rhs) {
	return lhs == rhs ? 0 : 1;
}


//STL min ����������� � Windows.h
#undef min

cost_type edit_distance(std::string const& from, std::string const& to) {
	std::vector<std::vector<cost_type>> costTable(from.length() + 1, std::vector<cost_type>(to.length() + 1));

	costTable[0][0] = 0;

	for (std::size_t j = 0; j != to.length(); ++j) {
		costTable[0][j + 1] = costTable[0][j] + insertion_cost(to[j]);
	}

	for (std::size_t i = 0; i != from.length(); ++i) {
		costTable[i + 1][0] = costTable[i][0] + removal_cost(from[i]);

		for (std::size_t j = 0; j != to.length(); ++j) {
			costTable[i + 1][j + 1] = std::min(std::min(
				costTable[i + 1][j] + insertion_cost(to[j]),
				costTable[i][j + 1] + removal_cost(from[i])),
				costTable[i][j] + update_cost(from[i], to[j]));
		}
	}

	return costTable[from.length()][to.length()];
}



void ACS::readKnown() {
	string s;
	ifstream f("known_plates.txt");
	while (!f.eof())
	{
		 f >> s;
		 known.push_back(s);
	}
	f.close();
}
void ACS::writeKnown(){
	string s;
	ofstream f("known_plates.txt");
	for (auto iter = known.begin(); iter < known.end(); iter++)
		f << *iter;
	f.close();
}

void ACS::run()
{
	ACS acs("C:\\Users\\Rudm\\Desktop\\Images\\");
	auto a = new thread(loop_in, &acs);
	auto b = new thread(loop_out, &acs);

	a->join();
	b->join();

	delete a;
	delete b;
}


void loop_in(ACS* acs)
{
	cvNamedWindow("Income", CV_WINDOW_NORMAL);
	cvResizeWindow("Income", 1024 / 2, 768 / 2);

	Recognizer rec;
	rec.openURL("http://192.168.2.10:9786/cameras/0/image", "vesy", "Vesy123");
	rec.recognize();

	while (true)
	{
		rec.readImage();
		rec.rotateImage(-3);
		string state = rec.getState();
		rec.drawTimestamp(state);
		rec.showImage("Income");

		if (state != "wait arrival"
			&& state != "wait departure"
			&& state != "searching..."
			&& state != "cleaning..."
			&& state != "unknown state")
		{
			state = acs->check(state);
			acs->arrival(state, rec);
		}
		waitForUserBreak(100);
	}
}


void loop_out(ACS* acs)
{
	cvNamedWindow("Outcome", CV_WINDOW_NORMAL);
	cvResizeWindow("Outcome", 1024 / 2, 768 / 2);

	Recognizer rec;
	rec.openURL("http://192.168.2.10:9786/cameras/3/image", "vesy", "Vesy123");
	rec.recognize();

	while (true)
	{
		rec.readImage();
		string state = rec.getState();
		rec.drawTimestamp(state);
		rec.showImage("Outcome");

		if (state != "wait arrival"
			&& state != "wait departure"
			&& state != "searching..."
			&& state != "cleaning..."
			&& state != "unknown state")
		{
			state = acs->check(state);
			acs->departure(state, rec);
		}
		waitForUserBreak(1000);
	}
}


string filename_ts(string plate, time_t sec )
{
	time_t seconds = sec;
	if (seconds == 0)
		seconds = time(NULL);
	tm timeinfo;
	localtime_s(&timeinfo, &seconds);


	stringstream  ss;
	ss << (timeinfo.tm_year+1900) << "_" << (timeinfo.tm_mon+1) << "_" << timeinfo.tm_mday << "_" << timeinfo.tm_hour << "_" << timeinfo.tm_min << "_" << timeinfo.tm_sec << "_" <<plate<<".jpg";
	return ss.str();
}
