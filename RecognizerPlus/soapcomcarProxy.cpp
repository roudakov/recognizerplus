/* soapcomcarProxy.cpp
   Generated by gSOAP 2.8.30 for recognize_soap.h

gSOAP XML Web services tools
Copyright (C) 2000-2016, Robert van Engelen, Genivia Inc. All Rights Reserved.
The soapcpp2 tool and its generated software are released under the GPL.
This program is released under the GPL with the additional exemption that
compiling, linking, and/or using OpenSSL is allowed.
--------------------------------------------------------------------------------
A commercial use license is available from Genivia Inc., contact@genivia.com
--------------------------------------------------------------------------------
*/

#include "soapcomcarProxy.h"

comcarProxy::comcarProxy() : soap(SOAP_IO_DEFAULT)
{	comcarProxy_init(SOAP_IO_DEFAULT, SOAP_IO_DEFAULT);
}

comcarProxy::comcarProxy(const comcarProxy& rhs)
{	soap_copy_context(this, &rhs);
	this->soap_endpoint = rhs.soap_endpoint;
}

comcarProxy::comcarProxy(const struct soap &_soap) : soap(_soap)
{ }

comcarProxy::comcarProxy(const char *endpoint) : soap(SOAP_IO_DEFAULT)
{	comcarProxy_init(SOAP_IO_DEFAULT, SOAP_IO_DEFAULT);
	soap_endpoint = endpoint;
}

comcarProxy::comcarProxy(soap_mode iomode) : soap(iomode)
{	comcarProxy_init(iomode, iomode);
}

comcarProxy::comcarProxy(const char *endpoint, soap_mode iomode) : soap(iomode)
{	comcarProxy_init(iomode, iomode);
	soap_endpoint = endpoint;
}

comcarProxy::comcarProxy(soap_mode imode, soap_mode omode) : soap(imode, omode)
{	comcarProxy_init(imode, omode);
}

comcarProxy::~comcarProxy()
{
	this->destroy();
	}

void comcarProxy::comcarProxy_init(soap_mode imode, soap_mode omode)
{	soap_imode(this, imode);
	soap_omode(this, omode);
	soap_endpoint = NULL;
	static const struct Namespace namespaces[] = {
        {"SOAP-ENV", "http://www.w3.org/2003/05/soap-envelope", "http://schemas.xmlsoap.org/soap/envelope/", NULL},
        {"SOAP-ENC", "http://www.w3.org/2003/05/soap-encoding", "http://schemas.xmlsoap.org/soap/encoding/", NULL},
        {"xsi", "http://www.w3.org/2001/XMLSchema-instance", "http://www.w3.org/*/XMLSchema-instance", NULL},
        {"xsd", "http://www.w3.org/2001/XMLSchema", "http://www.w3.org/*/XMLSchema", NULL},
        {"t", "http://tempuri.org/t.xsd", NULL, NULL},
        {"ns", "urn:comcar", NULL, NULL},
        {NULL, NULL, NULL, NULL}
    };
	soap_set_namespaces(this, namespaces);
}

#ifndef WITH_PURE_VIRTUAL
comcarProxy *comcarProxy::copy()
{	comcarProxy *dup = SOAP_NEW_COPY(comcarProxy(*(struct soap*)this));
	return dup;
}
#endif

comcarProxy& comcarProxy::operator=(const comcarProxy& rhs)
{	soap_copy_context(this, &rhs);
	this->soap_endpoint = rhs.soap_endpoint;
	return *this;
}

void comcarProxy::destroy()
{	soap_destroy(this);
	soap_end(this);
}

void comcarProxy::reset()
{	this->destroy();
	soap_done(this);
	soap_initialize(this);
	comcarProxy_init(SOAP_IO_DEFAULT, SOAP_IO_DEFAULT);
}

void comcarProxy::soap_noheader()
{	this->header = NULL;
}

::SOAP_ENV__Header *comcarProxy::soap_header()
{	return this->header;
}

::SOAP_ENV__Fault *comcarProxy::soap_fault()
{	return this->fault;
}

const char *comcarProxy::soap_fault_string()
{	return *soap_faultstring(this);
}

const char *comcarProxy::soap_fault_detail()
{	return *soap_faultdetail(this);
}

int comcarProxy::soap_close_socket()
{	return soap_closesock(this);
}

int comcarProxy::soap_force_close_socket()
{	return soap_force_closesock(this);
}

void comcarProxy::soap_print_fault(FILE *fd)
{	::soap_print_fault(this, fd);
}

#ifndef WITH_LEAN
#ifndef WITH_COMPAT
void comcarProxy::soap_stream_fault(std::ostream& os)
{	::soap_stream_fault(this, os);
}
#endif

char *comcarProxy::soap_sprint_fault(char *buf, size_t len)
{	return ::soap_sprint_fault(this, buf, len);
}
#endif

int comcarProxy::get_plate_number(const char *endpoint, const char *soap_action, std::string &result)
{	struct soap *soap = this;
	struct ns__get_plate_number soap_tmp_ns__get_plate_number;
	struct ns__get_plate_numberResponse *soap_tmp_ns__get_plate_numberResponse;
	if (endpoint)
		soap_endpoint = endpoint;
	if (soap_endpoint == NULL)
		soap_endpoint = "http://localhost:9999";
	if (soap_action == NULL)
		soap_action = "urn:get_plate_number";
	soap_begin(soap);
	soap_set_version(soap, 2); /* SOAP1.2 */
	soap->encodingStyle = "";
	soap_serializeheader(soap);
	soap_serialize_ns__get_plate_number(soap, &soap_tmp_ns__get_plate_number);
	if (soap_begin_count(soap))
		return soap->error;
	if (soap->mode & SOAP_IO_LENGTH)
	{	if (soap_envelope_begin_out(soap)
		 || soap_putheader(soap)
		 || soap_body_begin_out(soap)
		 || soap_put_ns__get_plate_number(soap, &soap_tmp_ns__get_plate_number, "ns:get-plate-number", NULL)
		 || soap_body_end_out(soap)
		 || soap_envelope_end_out(soap))
			 return soap->error;
	}
	if (soap_end_count(soap))
		return soap->error;
	if (soap_connect(soap, soap_endpoint, soap_action)
	 || soap_envelope_begin_out(soap)
	 || soap_putheader(soap)
	 || soap_body_begin_out(soap)
	 || soap_put_ns__get_plate_number(soap, &soap_tmp_ns__get_plate_number, "ns:get-plate-number", NULL)
	 || soap_body_end_out(soap)
	 || soap_envelope_end_out(soap)
	 || soap_end_send(soap))
		return soap_closesock(soap);
	if (!&result)
		return soap_closesock(soap);
	soap_default_std__string(soap, &result);
	if (soap_begin_recv(soap)
	 || soap_envelope_begin_in(soap)
	 || soap_recv_header(soap)
	 || soap_body_begin_in(soap))
		return soap_closesock(soap);
	if (soap_recv_fault(soap, 1))
		return soap->error;
	soap_tmp_ns__get_plate_numberResponse = soap_get_ns__get_plate_numberResponse(soap, NULL, "", NULL);
	if (!soap_tmp_ns__get_plate_numberResponse || soap->error)
		return soap_recv_fault(soap, 0);
	if (soap_body_end_in(soap)
	 || soap_envelope_end_in(soap)
	 || soap_end_recv(soap))
		return soap_closesock(soap);
	result = soap_tmp_ns__get_plate_numberResponse->result;
	return soap_closesock(soap);
}

int comcarProxy::open_url(const char *endpoint, const char *soap_action, std::string url, std::string user, std::string password, std::string &state)
{	struct soap *soap = this;
	struct ns__open_url soap_tmp_ns__open_url;
	struct ns__open_urlResponse *soap_tmp_ns__open_urlResponse;
	if (endpoint)
		soap_endpoint = endpoint;
	if (soap_endpoint == NULL)
		soap_endpoint = "http://localhost:9999";
	soap_tmp_ns__open_url.url = url;
	soap_tmp_ns__open_url.user = user;
	soap_tmp_ns__open_url.password = password;
	soap_begin(soap);
	soap_set_version(soap, 2); /* SOAP1.2 */
	soap->encodingStyle = "";
	soap_serializeheader(soap);
	soap_serialize_ns__open_url(soap, &soap_tmp_ns__open_url);
	if (soap_begin_count(soap))
		return soap->error;
	if (soap->mode & SOAP_IO_LENGTH)
	{	if (soap_envelope_begin_out(soap)
		 || soap_putheader(soap)
		 || soap_body_begin_out(soap)
		 || soap_put_ns__open_url(soap, &soap_tmp_ns__open_url, "ns:open-url", NULL)
		 || soap_body_end_out(soap)
		 || soap_envelope_end_out(soap))
			 return soap->error;
	}
	if (soap_end_count(soap))
		return soap->error;
	if (soap_connect(soap, soap_endpoint, soap_action)
	 || soap_envelope_begin_out(soap)
	 || soap_putheader(soap)
	 || soap_body_begin_out(soap)
	 || soap_put_ns__open_url(soap, &soap_tmp_ns__open_url, "ns:open-url", NULL)
	 || soap_body_end_out(soap)
	 || soap_envelope_end_out(soap)
	 || soap_end_send(soap))
		return soap_closesock(soap);
	if (!&state)
		return soap_closesock(soap);
	soap_default_std__string(soap, &state);
	if (soap_begin_recv(soap)
	 || soap_envelope_begin_in(soap)
	 || soap_recv_header(soap)
	 || soap_body_begin_in(soap))
		return soap_closesock(soap);
	if (soap_recv_fault(soap, 1))
		return soap->error;
	soap_tmp_ns__open_urlResponse = soap_get_ns__open_urlResponse(soap, NULL, "", NULL);
	if (!soap_tmp_ns__open_urlResponse || soap->error)
		return soap_recv_fault(soap, 0);
	if (soap_body_end_in(soap)
	 || soap_envelope_end_in(soap)
	 || soap_end_recv(soap))
		return soap_closesock(soap);
	state = soap_tmp_ns__open_urlResponse->state;
	return soap_closesock(soap);
}

int comcarProxy::send_close(const char *endpoint, const char *soap_action)
{	struct soap *soap = this;
	struct ns__close soap_tmp_ns__close;
	if (endpoint)
		soap_endpoint = endpoint;
	if (soap_endpoint == NULL)
		soap_endpoint = "http://localhost:9999";
	soap_begin(soap);
	soap_set_version(soap, 2); /* SOAP1.2 */
	soap->encodingStyle = "";
	soap_serializeheader(soap);
	soap_serialize_ns__close(soap, &soap_tmp_ns__close);
	if (soap_begin_count(soap))
		return soap->error;
	if (soap->mode & SOAP_IO_LENGTH)
	{	if (soap_envelope_begin_out(soap)
		 || soap_putheader(soap)
		 || soap_body_begin_out(soap)
		 || soap_put_ns__close(soap, &soap_tmp_ns__close, "ns:close", NULL)
		 || soap_body_end_out(soap)
		 || soap_envelope_end_out(soap))
			 return soap->error;
	}
	if (soap_end_count(soap))
		return soap->error;
	if (soap_connect(soap, soap_endpoint, soap_action)
	 || soap_envelope_begin_out(soap)
	 || soap_putheader(soap)
	 || soap_body_begin_out(soap)
	 || soap_put_ns__close(soap, &soap_tmp_ns__close, "ns:close", NULL)
	 || soap_body_end_out(soap)
	 || soap_envelope_end_out(soap)
	 || soap_end_send(soap))
		return soap_closesock(soap);
	return SOAP_OK;
}

int comcarProxy::recv_close(struct ns__close& tmp)
{	struct soap *soap = this;

	struct ns__close *_param_1 = &tmp;
	soap_default_ns__close(soap, _param_1);
	soap_begin(soap);
	if (soap_begin_recv(soap)
	 || soap_envelope_begin_in(soap)
	 || soap_recv_header(soap)
	 || soap_body_begin_in(soap))
		return soap_closesock(soap);
	soap_get_ns__close(soap, _param_1, "ns:close", NULL);
	if (soap->error == SOAP_TAG_MISMATCH && soap->level == 2)
		soap->error = SOAP_OK;
	if (soap->error
	 || soap_body_end_in(soap)
	 || soap_envelope_end_in(soap)
	 || soap_end_recv(soap))
		return soap_closesock(soap);
	return soap_closesock(soap);
}
/* End of client proxy code */
