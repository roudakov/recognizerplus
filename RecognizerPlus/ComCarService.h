#pragma once


#include <string>
#include "stdafx.h"
#include "stdsoap2.h"
#include "soapcomcarService.h"

#include "Recognizer.h"


class ComCarService : public  comcarService
{
private:
	Recognizer recognizer;

public: 
	virtual int open_url(std::string url, std::string user, std::string pass, std::string& state);
	virtual int close();
	virtual int get_plate_number(std::string& result);
};
