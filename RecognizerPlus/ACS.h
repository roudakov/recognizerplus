#pragma once
#include "Recognizer.h"

using namespace std;

class ACS
{
private:
	vector<string> known;
	vector<string> trip;

	string path;
public:
	ACS(string _path);
	~ACS();

	const string& check(const string& plate);

	void arrival(const string& plate, const Recognizer& rec);
	void departure(const string& plate, const Recognizer& rec);
	
	void print();


	static void run();


	void readKnown();
	void writeKnown();
private:
	friend void loop_in(ACS* acs);
	friend void loop_out(ACS* acs);
};



typedef double cost_type;
cost_type edit_distance(std::string const& from, std::string const& to);
const vector<string>::const_iterator find_morph(const vector<string>& list, string elem, cost_type delta = 2.0);
string filename_ts(string plate, time_t sec = 0);