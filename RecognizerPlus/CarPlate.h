#pragma once

#include <string>
#include <map>
using namespace std;

class CarPlate
{
private:
	string prefix;
	string number;
	string sufix;
	string region;

	bool   _valid;

public:
	CarPlate( const string& plate);
	~CarPlate();

	bool valid() const { return _valid; } ;
	string get() const { return prefix + number + sufix + region; }	
	
	const string& region_name() const;

private:
	void check_region();
	void check_letters();
	char check_letter(char c);
};

