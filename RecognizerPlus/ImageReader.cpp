#include "stdafx.h"
#include "ImageReader.h"
#include <curl/curl.h>
#include <iostream>



int ImageReaderURL::instances = 0;
inline IplImage* makeCvImage(char* buffer, size_t buffer_size);
size_t writeCallback(char *contents, size_t size, size_t nmemb, void *userdata);
void fill_cache(ImageReaderURLCache*obj);


ImageReaderFile::ImageReaderFile(string _filename)
{
	filename = _filename;
}
IplImage* ImageReaderFile::readImage()
{
	return cvLoadImage(filename.c_str(), CV_LOAD_IMAGE_COLOR);
}


ImageReaderDir::ImageReaderDir(string _path, string _mask):ImageReaderFile("")
{
	WCHAR str[500];
	MultiByteToWideChar(CP_OEMCP, MB_COMPOSITE, (_path+_mask).c_str(), -1, str, 500);

	path = _path;
	hFind = FindFirstFile(str, &FindFileData);

}
ImageReaderDir::~ImageReaderDir()
{
}
IplImage* ImageReaderDir::readImage()
{
	if (hFind == INVALID_HANDLE_VALUE)
		return nullptr;

	char str[500];
	WideCharToMultiByte(CP_OEMCP, WC_COMPOSITECHECK, FindFileData.cFileName, -1, str, 500, NULL, FALSE);

	filename = path + str;
	auto result = ImageReaderFile::readImage();
	if (!FindNextFile(hFind, &FindFileData))
	{
		FindClose(hFind);
		hFind = INVALID_HANDLE_VALUE;
	}
	return result;
}

ImageReaderURL::ImageReaderURL(string _url, string _username, string _password)
{
	url = _url;
	username = _username;
	password = _password;

	buffer = nullptr;
	buffer_size = 0;

	instances++;
	if (instances == 1)
		curl_global_init(CURL_GLOBAL_ALL);
}
ImageReaderURL::~ImageReaderURL()
{
	instances--;
	if (instances == 0)
		curl_global_cleanup();
}
IplImage* ImageReaderURL::readImage()
{
	IplImage*  result = nullptr;
	buffer = (char*)malloc(1);
	buffer_size = 0;
	CURL* curl = curl_easy_init();

	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, this->url.c_str());
		if (!username.empty())
		{
			curl_easy_setopt(curl, CURLOPT_USERNAME, this->username.c_str());
			curl_easy_setopt(curl, CURLOPT_PASSWORD, this->password.c_str());
			curl_easy_setopt(curl, CURLOPT_HTTPAUTH, (long)CURLAUTH_DIGEST);
		}

		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeCallback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)this);
		curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");


		/* Perform the request, res will get the return code */
		CURLcode res = curl_easy_perform(curl);
		/* Check for errors */
		if (res != CURLE_OK)
			cerr << "curl_easy_perform() failed: " << curl_easy_strerror(res) << endl;
		else
			result = makeCvImage(buffer, buffer_size);

		/* always cleanup */
		curl_easy_cleanup(curl);
	}

	free(buffer);
	buffer_size = 0;
	return result;
}


ImageReaderURLCache::ImageReaderURLCache(string _url, string _username, string _password):ImageReaderURL(_url, _username, _password)
{
	reader = new thread(fill_cache, this);
}
ImageReaderURLCache::~ImageReaderURLCache()
{
	done = true;
	reader->join();
	delete reader;

	ImageReaderURL::~ImageReaderURL();
}
IplImage* ImageReaderURLCache::readImage()
{
	while (cache.empty()) Sleep(10);

	IplImage* res = cache.front();
	cache.pop();

	if(cache.size()>0)
		cout << "query:" << cache.size() << endl;

	return res;
}
void ImageReaderURLCache::readImageCache()
{
	cache.push(ImageReaderURL::readImage());
}
void ImageReaderURLCache::sync()
{
	queue<IplImage*> empty;
	swap(cache, empty);
}


/*��������������� ���������*/
inline IplImage* makeCvImage(char* buffer, size_t buffer_size)
{
	auto mat = cvMat(1, buffer_size + 1, CV_8UC1, buffer);
	auto mat2 = cvCloneMat(&mat);
	auto img = cvDecodeImage(mat2);
	cvReleaseMat(&mat2);

	return img;
}
size_t writeCallback(char *contents, size_t size, size_t nmemb, void *userdata)
{
	auto obj = reinterpret_cast<ImageReaderURL*>(userdata);

	size_t realsize = size * nmemb;

	auto ptr = (char*)(realloc(obj->buffer, obj->buffer_size + realsize));
	if (ptr == nullptr) {
		/* out of memory! */
		cerr << "not enough memory (realloc returned NULL)" << endl;
		return 0;
	}
	else {
		obj->buffer = ptr;
	}
	memcpy(obj->buffer + obj->buffer_size, contents, realsize);
	obj->buffer_size += realsize;

	return realsize;
}
void fill_cache(ImageReaderURLCache*obj)
{
	while (!obj->done)
	{
		obj->readImageCache();
		Sleep(1000);
	}
}
