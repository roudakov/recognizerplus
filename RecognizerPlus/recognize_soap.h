#pragma once
#include <string>

//gsoap ns service name:	comcar
//gsoap ns service style:	rpc
//gsoap ns service encoding:	encoded
//gsoap ns service namespace:	urn:comcar
//gsoap ns service location:	http://localhost:9999
//gsoap ns service method-action: get_plate_number urn:get_plate_number

	typedef int xsd__int;
	typedef char* xsd__string;

	enum t__status				// remote status:
	{
		STATE_OK,					// ok
		STATE_FAIL				// fail to process
	};

	int ns__get_plate_number(std::string& result);
	int ns__open_url(std::string url, std::string user, std::string password, std::string& state );
	int ns__close(void);
