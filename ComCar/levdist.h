#pragma once

template <typename T>
typename T::size_type levenshtein_distance(const T & src, const T & dst);