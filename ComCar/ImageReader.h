#pragma once

#include <queue>
#include <thread>
#include <string>

#include <Windows.h>


#include "opencv2/highgui/highgui_c.h"

using namespace std;

class ImageReader
{
public:	
	virtual IplImage* readImage() = 0;
	virtual ~ImageReader() {};
};

class ImageReaderFile : public ImageReader
{
public:
	ImageReaderFile(string _filename);
	IplImage* readImage();

protected:
	string filename;
};

class ImageReaderDir : public ImageReaderFile
{
public:
	ImageReaderDir(string _path, string _mask);
	~ImageReaderDir();

	IplImage* readImage();

private:
	string path;
	WIN32_FIND_DATA FindFileData;
	HANDLE hFind;
};

class ImageReaderURL : public ImageReader
{
public:
	ImageReaderURL(string _url, string _username, string _password);
	~ImageReaderURL();


	IplImage* readImage();

private:
	string url;
	string username;
	string password;

	static int instances;

	/*����� ������ ��� ���*/
	char  *buffer;
	volatile size_t buffer_size;
	friend size_t writeCallback(char *contents, size_t size, size_t nmemb, void *userdata);
};


class ImageReaderURLCache : public ImageReaderURL
{
public:
	ImageReaderURLCache(string _url, string _username, string _password);
	~ImageReaderURLCache();

	void sync();

	IplImage* readImage();

private:

	thread*  reader;

	queue<IplImage*> cache;
	void readImageCache();
	bool done = false;

	friend void fill_cache(ImageReaderURLCache*obj);
};
