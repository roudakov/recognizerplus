#include "stdafx.h"
#include "Recognizer.h"
#include "CarPlate.h"
//#include "levdist.h"

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <ctime>

#include "iANPR.h"
#include "opencv2/highgui/highgui_c.h"



void Recognizer::readLicense()
{
	// ����� LicenseValue ��������� ������ ��� ������� ������
	// � ������ ���� ���, ����� ������ ��������������. 
	char key[1581] = { 0 };
	FILE* f;
	if (fopen_s(&f, "lic.key", "rb") == 0)
	{
		fread((void*)key, 1580, 1, f);
		fclose(f);
		LicenseValue(key);
	}
	else
		cerr << "WARNING! File lic.key not found. This may crash program if you use license version of iANPR SDK dlls" << endl;
}

Recognizer::Recognizer()
{
	//init ANPR parans
	param.Detect_Mode = ANPR_DETECTCOMPLEXMODE;
	param.min_plate_size = 500;
	param.max_plate_size = 50000;
	param.max_text_size = maxPlateNumbers;
	param.type_number = ANPR_RUSSIAN_EXTENDED;

	//init cache for plate numbers
	rects = new CvRect[maxPlatesOnImage];
	res = new char*[maxPlatesOnImage];
	for (int j = 0;j<maxPlatesOnImage;j++) res[j] = new char[maxPlateNumbers];

	//init thread
	flagStop = true;
	recognizeThread = nullptr;
	if (!InitializeCriticalSectionAndSpinCount(&CriticalSection, 0x00000400)) throw "Can't enter critical sections";

}
Recognizer::~Recognizer()
{
	//done thread
	stop(); // ������������� ���� �������������, ���� �� �������
	DeleteCriticalSection(&CriticalSection);

	//done cache for plate numbers
	for (int j = 0;j<maxPlatesOnImage;j++) delete[] res[j];
	delete[] res;
	delete[] rects;

	//done image cache
	freeFrame(false);
	if (plate_image)
		cvReleaseImage(&plate_image);

	//done reader
	if (reader) delete reader;
}

void Recognizer::freeFrame(bool copyPrev)
{
	if (prev_frame)
	{
		cvReleaseImage(&prev_frame);
		prev_frame = nullptr;
	}
	if (frame)
	{
		if (copyPrev)
			prev_frame = frame;
		else
			cvReleaseImage(&frame);
		frame = nullptr;
	}
}


void Recognizer::init(ImageReader* _reader) {
	if (reader) delete reader;
	reader = _reader;
}
bool Recognizer::openURL(const string& url, const string& username, const string& password)
{
	init(new ImageReaderURL(url, username, password));
	return readImage();
}
bool Recognizer::openFile(const string& filename)
{
	init( new ImageReaderFile(filename) );
	return readImage();
}

bool Recognizer::readImage()
{
	freeFrame();
	frame = reader->readImage();
	return frame != nullptr;
}
void Recognizer::saveImage(const string& filename) const
{
	int p[3];

	p[0] = CV_IMWRITE_JPEG_QUALITY;
	p[1] = 90;
	p[2] = 0;

	cvSaveImage(filename.c_str(), frame, p);
}
void Recognizer::showImage(const string& _windowName)  const {

	if (_windowName.empty())
	{
		//cvNamedWindow(windowName, CV_WINDOW_AUTOSIZE);
		cvShowImage(windowName, frame);
	}
	else
	{
		//cvNamedWindow(_windowName.c_str(), CV_WINDOW_AUTOSIZE);
		cvShowImage(_windowName.c_str(), frame);
	}
}

void Recognizer::hideImage(const string& _windowName)  const {
	if (_windowName.empty())
		cvDestroyWindow(windowName);
	else
		cvDestroyWindow(_windowName.c_str());
}
void Recognizer::rotateImage(double _angle)
{
	if (frame) {

		// ������� �������������
		CvMat* rot_mat = cvCreateMat(2, 3, CV_32FC1);
		// �������� ������������ ������ �����������
		CvPoint2D32f center = cvPoint2D32f(frame->width / 2, frame->height / 2);
		double angle = _angle;
		double scale = 1;
		cv2DRotationMatrix(center, angle, scale, rot_mat);

		IplImage* temp = cvCreateImage(cvSize(frame->width, frame->height), frame->depth, frame->nChannels);

		// ��������� ��������
		cvWarpAffine(frame, temp, rot_mat);

		// ��������� ���������
		cvCopy(temp, frame);

		cvReleaseImage(&temp);
		cvReleaseMat(&rot_mat);
	}
}
bool Recognizer::imageIsStill()  const
{

	if (!prev_frame) return true;
	if (!frame) return true;
	if (prev_frame->width != frame->width || prev_frame->height != frame->height) return false;

	IplImage* diff = cvCreateImage(cvGetSize(frame), frame->depth, frame->nChannels);
	IplImage* bin = cvCreateImage(cvGetSize(frame), frame->depth, frame->nChannels);
	cvZero(diff);
	cvZero(bin);

	cvAbsDiff(prev_frame, frame, diff);
	cvThreshold(diff, bin, 100 /* ����� */, 255, CV_THRESH_BINARY);

	double level = 0;
	for (int y = 0; y < bin->height; y++) {
		for (int x = 0; x < bin->width; x++) {
			level += static_cast<double> (CV_PIXEL(uchar, bin, x, y)[2]);
		}
	}

	level /= static_cast<double>(bin->width*bin->height);

	cvReleaseImage(&bin);
	cvReleaseImage(&diff);

	if (level > still_threshold)
		return false;
	else
		return true;
}

void Recognizer::clearResults()
{
	plates.clear();
	plates_rects.clear();
	plate = empty_string;
}
bool Recognizer::recognize(bool complex) {
	clearResults();
	return recognizeContinues(complex);
}
bool Recognizer::recognizeContinues(bool complex) {
	bool result = false;
	if (frame) {
		all = maxPlatesOnImage;

		const int ch = complex ? 4 : 1;
		IplImage** chanels = new IplImage*[ch];
		for (int i = 0; i < ch; i++)
		{
			chanels[i] = cvCreateImage(cvGetSize(frame), frame->depth, 1);
		}

		cvCvtColor(frame, chanels[ch - 1], CV_RGB2GRAY);
		if (complex) cvSplit(frame, chanels[0], chanels[1], chanels[2], nullptr);
		for (int i = 0; i < ch; i++)
		{
			int result_ianpr = anprPlate(chanels[i], param, &all, rects, res);
			if (result_ianpr == 0)
				for (int j = 0; j < all; j++) {
					double k = double(rects[j].width) / double(rects[j].height);
					int s = rects[j].height*rects[j].width;
					//	if (s >= minPlateArea && s <= maxPlateArea) {
					CarPlate pl(res[j]);
					if(pl.valid()) {
						result = true;
						plates.push_back(pl.get());
						plates_rects.push_back(rects[j]);
					}
				}
		}

		for (int i = 0; i < ch; i++)
			cvReleaseImage(&chanels[i]);
		delete[] chanels;
	}
	EnterCriticalSection(&CriticalSection);
	plate = majority(plates);
	LeaveCriticalSection(&CriticalSection);
	return result;
}

void Recognizer::waitForMotion() {

	EnterCriticalSection(&CriticalSection);
	state = "wait arrival";
	LeaveCriticalSection(&CriticalSection);

	while (imageIsStill() && plate.empty())
	{

		if (flagStop) return;

		Sleep(1000);
		readImage();
	}
}
void Recognizer::recognizeWhileCan() {

	EnterCriticalSection(&CriticalSection);
	state = "wait departure";
	LeaveCriticalSection(&CriticalSection);

	while (true) {
		if (flagStop) return;

		bool result = recognizeContinues();

		if(!result)	countNoCar++;
		if (countNoCar >= maxFrameForRecognition) { 
			//��������� ������ ������ ��� �������. ������� ��� � ���� ������������� ��� �����
			clearResults();
			countCar = 0;
			break;
		}
	}
}
void Recognizer::loop()
{
	while (true) {
		waitForMotion();
		recognizeWhileCan();

		if (flagStop) return;
	}
}


void Recognizer::start()
{
	flagStop = false;
	recognizeThread = new thread([this]() { this->loop(); });
}
void Recognizer::stop()
{
	if (recognizeThread != nullptr) {
		flagStop = true;
		recognizeThread->join();
		delete recognizeThread;
		recognizeThread = nullptr;
	}
}


void Recognizer::drawTimestamp(const string& msg)
{
	if (frame) {

		string timeStamp = timestamp() + (msg.empty() ? empty_string : (" - " + msg));

		CvFont font;
		float aa = 0.001f*frame->width;
		cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, aa,
			aa, 0, 1, 8);

		CvPoint pp2, pp1;
		pp2.x = 50;// frame->width-200;
		pp2.y = 50;//20;
		pp1.x = 51;//frame->width - 200 + 1;
		pp1.y = 51;//20 + 1;

		cvPutText(frame, timeStamp.c_str(), pp1, &font, CV_RGB(0, 0, 0));
		cvPutText(frame, timeStamp.c_str(), pp2, &font, CV_RGB(0, 255, 0));
	}

}

/*
void Recognizer::drawPlateNumbers()
{
	for (size_t j = 0; j < plates.size(); j++)
	{
		cvRectangle(frame, cvPoint(plates_rects[j].x, plates_rects[j].y), cvPoint(plates_rects[j].x + plates_rects[j].width,
			plates_rects[j].y + plates_rects[j].height), CV_RGB(255, 255, 0), 2);

		CvFont font;
		float aa = 0.002f*frame->width;
		cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, aa,
			aa, 0, 1, 8);
		CvPoint pp2, pp1;
		pp2.x = plates_rects[j].x;
		pp2.y = plates_rects[j].y;
		pp1.x = plates_rects[j].x + 1;
		pp1.y = plates_rects[j].y + 1;
		cvPutText(frame, plates[j].c_str(), pp1, &font, CV_RGB(0, 0, 0));
		cvPutText(frame, plates[j].c_str(), pp2, &font, CV_RGB(0, 255, 0));
	}
}
*/


string majority(vector<string> plates)  //�������� �������� ����� ������������� �����
{
	if (plates.empty()) return empty_string;

	//����� - https://habrahabr.ru/post/167177/
	int confidence = 0; 
	string candidate = empty_string; 
									 
	for (auto elem = plates.begin(); elem < plates.end(); elem++) {
		if (confidence == 0) {
			candidate = *elem;
			confidence++;
		}
		else if (candidate == *elem)
			confidence++;
		else
			confidence--;
	}

	return confidence > 0 ? candidate : plates.back();
}
string timestamp(time_t sec)
{
	time_t seconds = sec;
	if (seconds == 0)
		seconds = time(NULL);
	tm timeinfo;
	localtime_s(&timeinfo, &seconds);
	char timestamp[512];
	asctime_s(timestamp, 512, &timeinfo);
	timestamp[strnlen_s(timestamp, 512) - 1] = '\0';

	return string(timestamp);
}
const string empty_string;
static const char* windowName = "Camera";
