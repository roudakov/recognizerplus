// CarRecognizer.cpp: ���������� CCarRecognizer

#include "stdafx.h"
#include "CarRecognizer.h"
#include "atlbase.h"

// CCarRecognizer



STDMETHODIMP CCarRecognizer::get_plate(BSTR* pVal)
{
	CComBSTR out_str(recognizer.getPlate().c_str());

	*pVal = out_str.Detach();
	return S_OK;
}


STDMETHODIMP CCarRecognizer::get_minPlateArea(LONG* pVal)
{
	(*pVal) = recognizer.minPlateArea;

	return S_OK;
}


STDMETHODIMP CCarRecognizer::put_minPlateArea(LONG newVal)
{
	recognizer.minPlateArea = newVal;

	return S_OK;
}


STDMETHODIMP CCarRecognizer::get_maxPlateArea(LONG* pVal)
{
	(*pVal) = recognizer.maxPlateArea;

	return S_OK;
}


STDMETHODIMP CCarRecognizer::put_maxPlateArea(LONG newVal)
{
	recognizer.maxPlateArea = newVal;

	return S_OK;
}


STDMETHODIMP CCarRecognizer::openFile(BSTR filename)
{
	char str[500];
	WideCharToMultiByte(CP_OEMCP, WC_COMPOSITECHECK, filename, -1, str, 500, NULL, FALSE);

	if (recognizer.openFile(string(str)))
		return S_OK;
	else
		return S_FALSE;
}


STDMETHODIMP CCarRecognizer::openURL(BSTR url, BSTR username, BSTR password)
{
	char str_url[500];
	char str_user[500];
	char str_pass[500];
	WideCharToMultiByte(CP_OEMCP, WC_COMPOSITECHECK, url, -1, str_url, 500, NULL, FALSE);
	WideCharToMultiByte(CP_OEMCP, WC_COMPOSITECHECK, username, -1, str_user, 500, NULL, FALSE);
	WideCharToMultiByte(CP_OEMCP, WC_COMPOSITECHECK, password, -1, str_pass, 500, NULL, FALSE);

	if (recognizer.openURL(str_url, str_user, str_pass))
		return S_OK;
	else
		return S_FALSE;

	return S_OK;
}


STDMETHODIMP CCarRecognizer::saveImage(BSTR filename)
{
	char str[500];
	WideCharToMultiByte(CP_OEMCP, WC_COMPOSITECHECK, filename, -1, str, 500, NULL, FALSE);
	recognizer.saveImage(string(str));

	return S_OK;
}


STDMETHODIMP CCarRecognizer::rotateImage(FLOAT angle)
{
	recognizer.rotateImage(angle);

	return S_OK;
}


STDMETHODIMP CCarRecognizer::recognize()
{
	if (recognizer.recognize())
		return S_OK;
	else
		return S_FALSE;

	return S_OK;
}


STDMETHODIMP CCarRecognizer::showImage()
{
	recognizer.showImage();

	return S_OK;
}


STDMETHODIMP CCarRecognizer::hideImage()
{
	recognizer.hideImage();

	return S_OK;
}


STDMETHODIMP CCarRecognizer::drawPlate()
{
	//recognizer.drawPlateNumbers();

	return S_OK;
}


STDMETHODIMP CCarRecognizer::drawTimestamp()
{
	recognizer.drawTimestamp();

	return S_OK;
}


STDMETHODIMP CCarRecognizer::readLicense()
{
	recognizer.readLicense();

	return S_OK;
}


STDMETHODIMP CCarRecognizer::readImage()
{
	recognizer.readImage();
	return S_OK;
}


STDMETHODIMP CCarRecognizer::get_state(BSTR* pVal)
{
	CComBSTR out_str(recognizer.getState().c_str());

	*pVal = out_str.Detach();
	return S_OK;
}



STDMETHODIMP CCarRecognizer::startRecognize()
{
	// TODO: �������� ��� ����������
	recognizer.start();
	return S_OK;
}


STDMETHODIMP CCarRecognizer::stopRecognize()
{
	recognizer.stop();
	return S_OK;
}


STDMETHODIMP CCarRecognizer::get_hasPlateNumber(VARIANT_BOOL* pVal)
{
	// TODO: �������� ��� ����������
	*pVal = recognizer.hasPlate() ? VARIANT_TRUE : VARIANT_FALSE;
	return S_OK;
}
