#pragma once

#include "opencv2/highgui/highgui_c.h"
#include "iANPR.h"
#include "ImageReader.h"

#include <thread>

using namespace std;
extern const string empty_string;
extern const char* windowName;

class Recognizer
{
private:
	ImageReader* reader = nullptr;
	string plate;

public:
	Recognizer();
	~Recognizer();

	int minPlateArea = 5500;
	int maxPlateArea = 8900;

	void init(ImageReader* _reader);

	bool openFile(const string& filename);
	bool openURL(const string& url, const string& username, const string& password);

	bool readImage();
	void saveImage(const string& filename) const;
	void saveImagePlate(const string& filename) const;
	void rotateImage(double _angle);
	void showImage(const string& _windowName = empty_string) const;
	void hideImage(const string& _windowName = empty_string) const;

	bool imageIsStill() const;

	bool recognize(bool complex = false);
	bool recognizeContinues(bool complex = false);

	void drawTimestamp(const string& msg = empty_string);
	void drawPlateNumbers();

	static void readLicense();

	/*string operator [](int idx);
	int count();
	*/
	string getState() {
		EnterCriticalSection(&CriticalSection);
		auto result(state);
		LeaveCriticalSection(&CriticalSection);
		return result;
	}
	string getPlate() {
		EnterCriticalSection(&CriticalSection);
		auto result(plate);
		LeaveCriticalSection(&CriticalSection);
		return result;
	}
	bool hasPlate() { 
		EnterCriticalSection(&CriticalSection);
		auto result = !plate.empty();
		LeaveCriticalSection(&CriticalSection);
		return result;
	}

	bool flagStop;

private:
	double still_threshold = 1.0;

	static const int maxFrameForRecognition = 2;
	int countCar = 0;
	int countNoCar = 0;

	static const int maxPlatesOnImage = 100;
	static const int maxPlateNumbers = 20;

	IplImage* frame = nullptr;
	IplImage* prev_frame = nullptr;
	IplImage* plate_image = nullptr;

	vector<string> plates;
	vector<CvRect> plates_rects;

	/*��������� ��� ������� iANPR*/
	ANPR_OPTIONS param;
	CvRect* rects;
	int all = maxPlatesOnImage;
	char** res;

	void freeFrame(bool copyPrev = true);
	inline void clearResults();

//���� ������������ �������
private:
	string state;
	thread* recognizeThread;
	CRITICAL_SECTION CriticalSection;

	void loop();
	void waitForMotion();
	void recognizeWhileCan();

public:
	void start();
	void stop();
};



string timestamp(time_t sec = 0);
string majority(vector<string> plates);

#define waitForUserBreak(x) if (cvWaitKey(x) == 27) return;
#define waitForUserBreakB(x) if (cvWaitKey(x) == 27) break;

#define CV_PIXEL(type,img,x,y) (((type*)((img)->imageData+(y)*(img)->widthStep))+(x)*(img)->nChannels)


