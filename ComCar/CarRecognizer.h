// CarRecognizer.h: ���������� CCarRecognizer

#pragma once
#include "resource.h"       // �������� �������
#include "Recognizer.h"

#include "ComCar_i.h"



#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "������������� COM-������� �� �������������� ������� ������� ���������� Windows CE, �������� ����������� Windows Mobile, � ������� �� ������������� ������ ��������� DCOM. ���������� _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA, ����� ��������� ATL ������������ �������� ������������� COM-�������� � ��������� ������������� ��� ���������� ������������� COM-��������. ��� ��������� ������ � ����� rgs-����� ������ �������� 'Free', ��������� ��� ������������ ��������� ������, �������������� ��-DCOM ����������� Windows CE."
#endif

using namespace ATL;


// CCarRecognizer

class ATL_NO_VTABLE CCarRecognizer :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CCarRecognizer, &CLSID_CarRecognizer>,
	public IDispatchImpl<ICarRecognizer, &IID_ICarRecognizer, &LIBID_ComCarLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CCarRecognizer()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_CARRECOGNIZER)


BEGIN_COM_MAP(CCarRecognizer)
	COM_INTERFACE_ENTRY(ICarRecognizer)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
	Recognizer recognizer;


	STDMETHOD(get_plate)(BSTR* pVal);
	STDMETHOD(get_minPlateArea)(LONG* pVal);
	STDMETHOD(put_minPlateArea)(LONG newVal);
	STDMETHOD(get_maxPlateArea)(LONG* pVal);
	STDMETHOD(put_maxPlateArea)(LONG newVal);
	STDMETHOD(openFile)(BSTR filename);
	STDMETHOD(openURL)(BSTR url, BSTR username, BSTR password);
	STDMETHOD(saveImage)(BSTR filename);
	STDMETHOD(rotateImage)(FLOAT angle);
	STDMETHOD(recognize)();
	STDMETHOD(showImage)();
	STDMETHOD(hideImage)();
	STDMETHOD(drawPlate)();
	STDMETHOD(drawTimestamp)();
	STDMETHOD(readLicense)();
	STDMETHOD(readImage)();
	STDMETHOD(get_state)(BSTR* pVal);
	STDMETHOD(startRecognize)();
	STDMETHOD(stopRecognize)();
	STDMETHOD(get_hasPlateNumber)(VARIANT_BOOL* pVal);
};

OBJECT_ENTRY_AUTO(__uuidof(CarRecognizer), CCarRecognizer)
